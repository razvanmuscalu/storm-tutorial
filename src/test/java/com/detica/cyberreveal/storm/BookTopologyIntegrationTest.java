package com.detica.cyberreveal.storm;

import com.detica.cyberreveal.storm.topology.BookTopology;
import com.detica.cyberreveal.storm.util.TestUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BookTopologyIntegrationTest {

    private BookTopology testBookTopology;
    private static final String TEST_WORD_COUNT_FILE = "target/wordCounts.out";
    private static final String TEST_WORD_RANK_FILE = "target/wordRanks.out";
    File wordCountFile = new File(TEST_WORD_COUNT_FILE);
    File wordRankFile = new File(TEST_WORD_RANK_FILE);

    @Before
    public void setUp() {
        if (wordCountFile.exists())
            wordCountFile.delete();
        if (wordRankFile.exists())
            wordRankFile.delete();

        testBookTopology = new BookTopology(null, "src/main/resources/AdventuresOfSherlockHolmes.txt");

        testBookTopology.run();
    }

    @Test
    @Ignore
    public void shouldOutputMaximumCountForSpecificWordTest() {
        String output = TestUtils.readFile(TEST_WORD_COUNT_FILE).toString();
        assertTrue(output.toString().contains("holmes, 48"));
        assertFalse(output.toString().contains("holmes, 49"));
    }

    @Test
    @Ignore
    public void shouldOutputRankForSpecificWordTest() {
        String output = TestUtils.readFile(TEST_WORD_RANK_FILE).toString();
        assertTrue(output.toString().contains("holmes, 1"));
    }
}
