package com.detica.cyberreveal.storm.bolt;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.detica.cyberreveal.storm.util.TestUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class WordCountBoltTest {

    private WordCountBolt wordCountBolt;
    private BasicOutputCollector basicOutputCollector;
    private OutputFieldsDeclarer outputFieldsDeclarer;
    private Tuple wordTuple;

    @Before
    public void setUp() {
        wordCountBolt = new WordCountBolt();
        basicOutputCollector = mock(BasicOutputCollector.class);
        outputFieldsDeclarer = mock(OutputFieldsDeclarer.class);
        wordTuple = TestUtils.mockStandardTuple();
    }

    @Test
    public void shouldEmitNewValuesIfWordWasCountedTest() {
        // when
        wordCountBolt.execute(wordTuple, basicOutputCollector);

        // then
        verify(basicOutputCollector).emit(any(Values.class));
        verifyNoMoreInteractions(basicOutputCollector);
    }

    @Test
    public void shouldIncreaseCountByOneIfWordWasCountedTest() {
        // given
        when(wordTuple.getStringByField("word")).thenReturn("cat");

        // when
        wordCountBolt.execute(wordTuple, basicOutputCollector);

        // then
        assertTrue(wordCountBolt.getWordCounts().get("cat") == 1);
    }

    @Test
    public void shouldDeclareOutputFieldsTest() {
        // when
        wordCountBolt.declareOutputFields(outputFieldsDeclarer);

        // then
        verify(outputFieldsDeclarer, times(1)).declare(any(Fields.class));
        verifyNoMoreInteractions(outputFieldsDeclarer);
    }
}
