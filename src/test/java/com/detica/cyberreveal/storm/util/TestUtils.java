package com.detica.cyberreveal.storm.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import backtype.storm.tuple.Tuple;
import static org.mockito.Mockito.*;

public final class TestUtils {

	private static final Logger LOG = LoggerFactory.getLogger(TestUtils.class);
	
	public static final int SYSTEM_TASK_ID = 1;
    public static final String SYSTEM_STREAM_ID = "__stream";	
    
    private static FileInputStream fis;
    private static BufferedReader in;
	
    private TestUtils() {
    }

    public static Tuple mockStandardTuple() {
    	return mockTuple(SYSTEM_TASK_ID, SYSTEM_STREAM_ID);
    }

    public static Tuple mockTuple(int taskId, String streamId) {
    	Tuple tuple = mock(Tuple.class);
    	when(tuple.getSourceTask()).thenReturn(taskId);
    	when(tuple.getSourceStreamId()).thenReturn(streamId);
    	return tuple;
    }
    
    public static StringBuilder readFile(String wordCountFile) {
    	StringBuilder output = new StringBuilder();
		try { 							 	        
			 fis =new FileInputStream(wordCountFile);
			 in = new BufferedReader(new InputStreamReader(fis));
		    String line = null;
		    while((line = in.readLine()) != null) {
		        	output.append(line); 
		        }
		} catch (FileNotFoundException e1) {
			LOG.error("FileNotFoundException in .util.TestUtils.readFile", e1);
		} catch (IOException e2) {
			
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				LOG.error("IOException in .util.TestUtils.readFile", e);
			}
		}
		return output;
    }
}
