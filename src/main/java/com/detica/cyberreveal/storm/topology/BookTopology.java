package com.detica.cyberreveal.storm.topology;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.utils.Utils;
import com.detica.cyberreveal.storm.bolt.FilePrinterBolt;
import com.detica.cyberreveal.storm.bolt.WordCountBolt;
import com.detica.cyberreveal.storm.bolt.WordRankBolt;
import com.detica.cyberreveal.storm.bolt.WordSplitBolt;
import com.detica.cyberreveal.storm.spout.BookLineSpout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * The Class TestTopology.
 */
public final class BookTopology implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(BookTopology.class);
    private final String topologyName;
    private final String inputFile;

    /**
     * Instantiates a new book topology.
     *
     * @param topologyName the topology name
     */
    public BookTopology(String topologyName, String inputFile) {
        this.topologyName = topologyName;
        this.inputFile = inputFile;
    }

    @Override
    public void run() {
        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("line", new BookLineSpout(), 1);
        builder.setBolt("wordSplitter", new WordSplitBolt(), 2).shuffleGrouping("line");
        builder.setBolt("wordCount", new WordCountBolt(), 2).fieldsGrouping("wordSplitter", new Fields("word"));
        builder.setBolt("wordRank", new WordRankBolt(), 2).fieldsGrouping("wordCount", new Fields("word"));
        builder.setBolt("printWordCountToFile", new FilePrinterBolt(new File("target/wordCounts.out")), 2).shuffleGrouping("wordCount");
        builder.setBolt("printWordRankToFile", new FilePrinterBolt(new File("target/wordRanks.out")), 2).shuffleGrouping("wordRank");

        Config conf = new Config();
        conf.setDebug(true);
        conf.put("inputFile", this.inputFile);
        if (this.topologyName != null) {
            conf.setNumWorkers(20);

            try {
                StormSubmitter.submitTopology(this.topologyName, conf, builder.createTopology());
            } catch (AlreadyAliveException e) {
                LOG.error("AlreadyAliveException in .topology.BookTopology.run", e);
            } catch (InvalidTopologyException e) {
                LOG.error("InvalidTopologyException in .topology.BookTopology.run", e);
            }
        } else {
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology("test", conf, builder.createTopology());
            Utils.sleep(10000);
            cluster.killTopology("test");
            cluster.shutdown();
        }
    }
}
