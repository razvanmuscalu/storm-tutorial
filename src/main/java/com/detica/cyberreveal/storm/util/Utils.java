package com.detica.cyberreveal.storm.util;

import com.detica.cyberreveal.storm.spout.BookLineSpout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    private static final Logger LOG = LoggerFactory.getLogger(BookLineSpout.class);

    public static List<String> extractLinesFromFile(File inputFile) {
        List<String> lines = new ArrayList<String>();
        BufferedReader buff = null;
        try {
            FileReader inStream = new FileReader(inputFile);
            buff = new BufferedReader(inStream);
            String line = null;
            while ((line = buff.readLine()) != null) {
                lines.add(line);
            }
        } catch (FileNotFoundException e) {
            LOG.error("FileNotFoundException in .util.Utils.extractLinesFromFile", e);
        } catch (IOException e) {
            LOG.error("IOException in .util.Utils.extractLinesFromFile", e);
        } finally {
            try {
                buff.close();
            } catch (IOException e) {
                LOG.error("IOException in .util.Utils.extractLinesFromFile", e);
            }
        }
        return lines;
    }
}
