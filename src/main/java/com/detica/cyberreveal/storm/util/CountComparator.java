package com.detica.cyberreveal.storm.util;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;

public class CountComparator implements Serializable, Comparator {

    private static final long serialVersionUID = -7142877249756086117L;

    Map map;

    public CountComparator(Map map) {
        this.map = map;
    }

    public int compare(Object keyA, Object keyB) {
        Comparable valueA = (Comparable) map.get(keyA);
        Comparable valueB = (Comparable) map.get(keyB);
        return valueB.compareTo(valueA);
    }
}
