package com.detica.cyberreveal.storm.bolt;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * a Storm Bolt which appends all recieved tuples to a specified file.
 */
public class FilePrinterBolt extends BaseBasicBolt {

    private static final Logger LOG = LoggerFactory.getLogger(FilePrinterBolt.class);
    private static final long serialVersionUID = 327323938874334973L;
    private File outputFile;
    private FileWriter writer;

    /**
     * Instantiates a new file printer bolt.
     *
     * @param outputFile the output file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public FilePrinterBolt(final File outputFile) {
        this.outputFile = outputFile;
    }

    @Override
    public void execute(final Tuple tuple, final BasicOutputCollector collector) {
        System.out.println(tuple);
        try {
            writer = new FileWriter(this.outputFile, true);
            writer.append(tuple.toString() + "\n");
            writer.close();
        } catch (IOException e) {
            LOG.error("IOException in .bolt.FilePrinterBolt.execute", e);
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                LOG.error("IOException in .bolt.FilePrinterBolt.execute", e);
            }
        }
    }

    @Override
    public void declareOutputFields(final OutputFieldsDeclarer ofd) {
    }

}
