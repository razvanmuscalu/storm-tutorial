package com.detica.cyberreveal.storm.bolt;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.detica.cyberreveal.storm.util.CountComparator;
import com.dictiography.collections.IndexedTreeMap;

import java.util.HashMap;
import java.util.Map;

public class WordRankBolt extends BaseBasicBolt {

    private static final long serialVersionUID = 4557306751596014015L;
    private final Map<String, Long> wordCounts = new HashMap<String, Long>();
    private CountComparator countComparator = new CountComparator(wordCounts);
    private IndexedTreeMap<String, Long> sortedWordCounts = new IndexedTreeMap<String, Long>(countComparator);

    @Override
    public void execute(final Tuple tuple, final BasicOutputCollector collector) {
        String word = tuple.getStringByField("word");
        Long count = tuple.getLongByField("count");
        this.wordCounts.put(word, count);
        sortedWordCounts.putAll(wordCounts);
        collector.emit(new Values(word, sortedWordCounts.keyIndex(word) + 1));
    }

    @Override
    public void declareOutputFields(final OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("word", "rank"));
    }

}
