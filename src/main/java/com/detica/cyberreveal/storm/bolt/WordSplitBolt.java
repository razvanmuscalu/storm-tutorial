package com.detica.cyberreveal.storm.bolt;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A storm bolt which splits a line into words.
 */
public class WordSplitBolt extends BaseBasicBolt {

    private static final long serialVersionUID = 1990152678196466476L;

    Pattern pattern = Pattern.compile("\\w+");

    @Override
    public void execute(final Tuple tuple, final BasicOutputCollector collector) {
        String line = tuple.getStringByField("line");
        // split line by whitespace and punctuation characters
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            String word = matcher.group().toLowerCase().trim();
            collector.emit(new Values(word));
        }
    }

    @Override
    public void declareOutputFields(final OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("word"));
    }

}
