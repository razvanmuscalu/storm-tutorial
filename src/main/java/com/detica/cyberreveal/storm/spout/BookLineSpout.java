package com.detica.cyberreveal.storm.spout;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A storm spout which reads a file and outputs each line to a spearate tuple.
 */
public class BookLineSpout extends BaseRichSpout {

    private static final long serialVersionUID = -7281111950770566776L;

    private SpoutOutputCollector collector;
    private List<String> lines;

    @Override
    public void open(@SuppressWarnings("rawtypes") final Map conf, final TopologyContext context, final SpoutOutputCollector spoutCollector) {
        this.lines = new ArrayList<String>();
        this.collector = spoutCollector;
        // Read input file, one line at a time, and add each line to a list
        File inputFile = new File((String) conf.get("inputFile"));
        this.lines.addAll(com.detica.cyberreveal.storm.util.Utils.extractLinesFromFile(inputFile));
    }

    @Override
    public void nextTuple() {
        if (!this.lines.isEmpty()) {
            String line = this.lines.remove(0);
            this.collector.emit(new Values(line));
        }
    }

    @Override
    public void ack(final Object id) {
    }

    @Override
    public void fail(final Object id) {
    }

    @Override
    public void declareOutputFields(final OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("line"));
    }

}
