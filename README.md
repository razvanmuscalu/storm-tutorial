# Apache Storm 

# Overview

A simple topology that reads from the source (line by line): src/main/resources/AdventuresOfSherlockHolmes.txt 

It produces two outputs:
- word-count pairs in: target/wordCounts.out
- word-rank pairs (based on count) in: target/wordRanks.out

Topology Structure [master]

![bookTopology](/hld.png)

Topology Structure [with_storm_starter]

Same design as above, but WordRankBolt becomes IntermediateRankingsBolt
                                                                                              
# Further Documentation

A report can be found in the root of the repository under 'master' branch: Storm_Tutorial_Report.txt

# Notes

Please be aware that the 'master' branch makes use of a third-party library: indexed-tree-map.jar

This can be found at: https://code.google.com/p/indexed-tree-map/downloads/list

I imported it into my local Maven repository (in the absence of a Nexus environment): /.m2/repository/thirdparty/indexed-tree-map/1.0/indexed-tree-map-1.0.jar
